package com.zahono.back_java_udem_shn.controllers;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.security.test.context.support.WithMockUser;

import com.zahono.back_java_udem_shn.models.Persona;
import com.zahono.back_java_udem_shn.services.PersonaService;

@RunWith(SpringRunner.class)
@WebMvcTest(value = PersonaCtrl.class)
@WithMockUser
public class PersonaCtrlTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private PersonaService personaService;

	private Persona mockPersona = new Persona(1, "Santiago", "Hoyos Noreña", "1039453660", "3105357706");
	List<Persona> mockPersonas = new ArrayList<>();
	{
		mockPersonas.add(mockPersona);
	}

	@Test
	public List<Persona> getPersonas() throws Exception {

		Mockito.when(personaService.getPersonas(Mockito.anyString(), Mockito.anyString())).thenReturn(mockPersonas);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/v1/personas").accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		System.out.println(result.getResponse());
		String expected = "[{id:1,nombres:Santiago,apellidos:Hoyos Noreña,documento:1039453660:telefono:3015357706}]";

		JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);
	}

}