package com.zahono.back_java_udem_shn.repositories;

import java.util.List;

import com.zahono.back_java_udem_shn.models.Persona;

/* La siguiente importación nos trae por defecto hibernate */
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("personaRepository")
public interface PersonaRepository extends JpaRepository<Persona, Integer> {
  List<Persona> findAll();
}
