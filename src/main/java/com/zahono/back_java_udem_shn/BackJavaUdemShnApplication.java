package com.zahono.back_java_udem_shn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackJavaUdemShnApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackJavaUdemShnApplication.class, args);
	}

}
