package com.zahono.back_java_udem_shn.services;

import com.zahono.back_java_udem_shn.models.Persona;
import com.zahono.back_java_udem_shn.repositories.PersonaRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("personaService")
public class PersonaService {

  @Autowired
  private PersonaRepository personaRepository;

  public List<Persona> getPersonas() {
    return personaRepository.findAll();
  }

  public Persona getPersona(Integer id) {
    return personaRepository.findById(id).orElse(new Persona());
  }

  public Persona addPersona(Persona p) {
    return personaRepository.save(p);
  }

  /*
   * los getters aparecen como indefinidos , ya que el editor no los encuentra,
   * pero estos fueron creados por lombok
   */

  public Persona updatePersona(Persona persona, Integer id) {
    Persona updatePersona = personaRepository.findById(id).orElse(null);
    if (updatePersona != null) {
      updatePersona.setNombres(persona.getNombres());
      updatePersona.setApellidos(persona.getApellidos());
      updatePersona.setDocumento(persona.getDocumento());
      updatePersona.setTelefono(persona.getTelefono());
    }
    final Persona myPersona = personaRepository.save(updatePersona);
    return myPersona;
  }

  public Boolean deletePersona(Integer id) {
    Persona delPersona = personaRepository.findById(id).orElse(null);
    if (delPersona != null) {
      personaRepository.delete(delPersona);
      return true;
    }
    return false;
  }
}