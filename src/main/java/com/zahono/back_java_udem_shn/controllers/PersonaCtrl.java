package com.zahono.back_java_udem_shn.controllers;

import com.zahono.back_java_udem_shn.models.Persona;
import com.zahono.back_java_udem_shn.services.PersonaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

import javax.validation.Valid;

/**
 * ApiCtrl
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping({ "/api/v1" })
public class PersonaCtrl {

  @Autowired
  private PersonaService personaService;

  /* GETS */

  @GetMapping("/personas")
  public List<Persona> List() {
    return personaService.getPersonas();
  }

  @GetMapping("/personas/{id}")
  public Persona ListId(@PathVariable Integer id) {
    return personaService.getPersona(id);
  }

  /* POSTS */

  @PostMapping("/personas")
  public ResponseEntity<Persona> createPersona(@RequestBody Persona p) {
    return ResponseEntity.ok(personaService.addPersona(p));
  }

  /* PUTS */

  @PutMapping("/personas/{id}")
  public ResponseEntity<Persona> updateMember(@Valid @RequestBody Persona p, @PathVariable(value = "id") Integer id) {
    return ResponseEntity.ok(personaService.updatePersona(p, id));
  }

  /* DELETES */

  @DeleteMapping("/personas/{id}")
  public ResponseEntity<?> deleteMemeber(@PathVariable Integer id) {
    Map<String, String> response = new HashMap<String, String>();
    if (personaService.deletePersona(id)) {
      response.put("status", "success");
      response.put("message", "Persona eliminada exitosamente");
      return ResponseEntity.ok(response);
    } else {
      response.put("status", "error");
      response.put("message", "Hubo un error al eliminar a la personaa con id: " + id);
      return ResponseEntity.status(500).body(response);
    }
  }

}