package com.zahono.back_java_udem_shn.models;

import javax.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "personas")
public class Persona {

  @Id
  @Column
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  Integer id;

  @Column(length = 50, nullable = false)
  String nombres;

  @Column(length = 50, nullable = false)
  String apellidos;

  @Column(unique = true, nullable = false)
  Long documento;

  @Column
  Long telefono;

}
